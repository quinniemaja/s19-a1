  let student1 = {
	name: "Shawn Michaels",
	birthday: "May 5, 2003",
	age: 18,
	isEnrolled: true,
	classes: ["Philosphy 101", "Social Sciences 201"]
};

let student2 = {
	name : "Steve Austin",
	birthday: "June 15, 2001",
	age: 20,
	isEnrolled: true,
	classes: ["Philosphy 401", "Natural Sciences 402"],
}

const {name, birthday, age, isEnrolled, classes} = student1;
const {name1, birthday1, age1, isEnrolled1, classes1} = student2;


const greet = (student) => { //for object literals
	console.log(`Hi! I'm ${student.name}. I am ${student.age} years old.`);
	console.log(`I study the following courses ${student.classes}`);
};

greet(student1);
greet(student2);

// function getCube(num){

// 	console.log(Math.pow(num,3));

// }

const getCube = (num) => console.log(Math.pow(num,3));
getCube(3);


let numArr = [15,16,32,21,21,2]

numArr.forEach(num => console.log(num));

let numSquared = numArr.map(num => {return num **2});
console.log(numSquared);

//#2 

class pet {
	constructor (name,breed,dogAge) {
		this.name = name;
		this.breed = breed;
		this.dogAge = (7*dogAge);
	}
};

let pet1 = new pet('Maggie', 'shitzu', 3);
console.log(pet1);

//#3 
class dog {
	constructor (name, breed, gender) {
		this.name = name,
		this.breed = breed,
		this.gender = gender
	}
};

let dog1 = new dog('tricia', 'shitzu', 'female');
let dog2 = new dog('yuki', 'husky', 'male');

console.log(dog1);
console.log(dog2);
